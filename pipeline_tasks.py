#!/usr/bin/env python

from subprocess import call
from sys import exc_info, exit


def get_pipeline_tasks(pipelines: list, selected_pipeline: str):
    """
    Returns the list of tasks that a pipeline can execute
    :return:
    """
    return next(pipeline[selected_pipeline] for pipeline in pipelines if selected_pipeline in pipeline)


def get_task(tasks: list, given_task: str):
    """
    Returns a given task from the tasks list
    :param tasks:
    :param given_task:
    :return:
    """
    return next(task[given_task] for task in tasks if given_task in task)


def execute_tasks(parsed_data: dict, selected_pipeline: str):
    """
    Execute taks from a selected pipeline
    :param parsed_data:
    :param selected_pipeline:
    :return:
    """
    for task in get_pipeline_tasks(parsed_data.get('pipelines'), selected_pipeline):
        if task == 'invalid':
            print("Error: The pipeline you chose doesn't have a valid command list.")
            exit(1)

        task_cmd = get_task(parsed_data.get('tasks'), task).get('cmd')

        try:
            call(task_cmd.split(" "))
        except:
            err = exc_info()[1]
            print("Error: It was not possible to execute the task command\nCommand: %s\nMessage: %s" % (task_cmd, err))
            exit(1)
