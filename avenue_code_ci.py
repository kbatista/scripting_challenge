#!/usr/bin/env python

from yaml import load
from git_functions import clone_repo, fetch_repo
from pipeline_tasks import execute_tasks
import os
import argparse
from sys import exc_info, exit

parser = argparse.ArgumentParser()

parser.add_argument('--pipeline', help='The name of the desired Pipeline.')
parser.add_argument('--repository', help='URL of the git repository to be integrated.')

options = parser.parse_args()

WORKSPACE = 'workspace'
GIVEN_PIPELINE = options.pipeline
GIT_URL = options.repository
REPO_NAME = GIT_URL.rsplit("/", 1)[1]
REPO_PATH = "%s/%s.tmp/" % (WORKSPACE, REPO_NAME)


def yaml_parser():
    """
    Parses the yaml file
    :return: yaml parsed as dict
    """
    try:
        return load(open(REPO_PATH + 'pipeline.yml', 'r'))

    except:
        err = exc_info()[1]
        print("Error: It was not possible to parse yaml file\nMessage: %s" % err)
        exit(1)


def set_workdir():
    """
    Sets the working dir to the cloned repository
    :return:
    """
    os.chdir(REPO_PATH)


if __name__ == "__main__":
    if not os.path.isdir(WORKSPACE):
        print("Creating workspace...")
        os.mkdir(WORKSPACE)

    clone_repo(REPO_PATH, GIT_URL)

    parsed_yaml = yaml_parser()

    fetch_repo(parsed_yaml.get('branch'), REPO_PATH)

    set_workdir()

    execute_tasks(parsed_yaml, GIVEN_PIPELINE)
