FROM centos:7

# Add requirements to install script dependencies
ADD requirements.txt /tmp/requirements.txt

# Environment vars
ENV PYTHON_VERSION="3.5.3"
ENV GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# Install image requirements
RUN yum -y install \
        epel-release \
        git-core \
        maven \
        zip \
        unzip \
        java-1.8.0-openjdk \
    && yum clean all

# Install required packages
RUN yum update -y; yum clean all

RUN yum-builddep -y python; yum -y install make postgresql-devel gcc \
 libtiff-devel libjpeg-devel libzip-devel freetype-devel lcms2-devel libwebp-devel tcl-devel tk-devel \
 libxslt-devel libxml2-devel; yum clean all

# Downloading and building python
RUN mkdir /tmp/python-build && cd /tmp/python-build && \
  curl https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz > python.tgz && \
  tar xzf python.tgz && cd Python-$PYTHON_VERSION && \
  ./configure --prefix=/usr/local && make install && cd / && rm -rf /tmp/python-build


WORKDIR /tmp

RUN pip3 install -r requirements.txt

# Creates group and user that will be used by the container
RUN groupadd -g 1000 appuser
RUN useradd -u 1000 -g 1000 appuser

# Create app dir and give ownership to previous created user
RUN mkdir /app; \
    chown -R appuser:appuser /app

USER appuser