#!/usr/bin/env python

from shutil import rmtree
from os import path, mkdir
from sys import exc_info, exit
from subprocess import call


def clone_repo(repo_path: str, repo_url: str, quiet=False):
    """
    Clone repository using the given repo url
    :param quiet:
    :param repo_path:
    :param repo_url:
    :return:
    """
    try:
        if path.isdir(repo_path):
            rmtree(repo_path)
        else:
            mkdir(repo_path)

        if quiet:
            call(["git", "clone", "-q", repo_url, repo_path])
        else:
            call(["git", "clone", repo_url, repo_path])
    except:
        err = exc_info()[1]
        print("Error: It was not possible to clone the repository\nMessage: %s" % err)
        exit(1)


def fetch_repo(branch: str, repo_path: str, quiet=False):
    """
    Fetch repository and checkout to specified branch
    :param quiet:
    :param repo_path:
    :param branch:
    :return:
    """
    try:
        if quiet:

            call(["git", "fetch", "--all", "-q"])

            call(["git", "--git-dir", repo_path + ".git", "checkout", branch, "-q"])
        else:

            call(["git", "fetch", "--all"])

            call(["git", "--git-dir", repo_path + ".git", "checkout", branch])
    except:
        err = exc_info()[1]
        print("Error: It was not possible to fetch the repository\nMessage: %s" % err)
        exit(1)
