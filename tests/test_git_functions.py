import unittest
import git_functions
from shutil import rmtree
from os import mkdir


class GitFunctionsTests(unittest.TestCase):
    def test_clone_repo(self):
        """
        Should return 0 if git repo is successfully cloned.
        :return:
        """
        tmp_dir = '.tmp/'
        mkdir(tmp_dir)

        repo_url = 'https://bitbucket.org/ac-recruitment/scripting-helloworld.git'
        repo_name = repo_url.rsplit("/", 1)[1]
        repo_path = "%s%s" % (tmp_dir, repo_name)

        self.assertEqual(git_functions.clone_repo(repo_path, repo_url, True), None)

        rmtree(tmp_dir)

    def test_fetch_repo(self):
        """
        Should return 0 if fetch and branch checkout runs successfully.
        :return:
        """
        tmp_dir = '.tmp/'
        mkdir(tmp_dir)

        repo_url = 'https://bitbucket.org/ac-recruitment/scripting-helloworld.git'
        repo_name = repo_url.rsplit("/", 1)[1]
        repo_path = "%s%s/" % (tmp_dir, repo_name)
        branch = 'master'

        git_functions.clone_repo(repo_path, repo_url, True)

        self.assertEqual(git_functions.fetch_repo(branch, repo_path, True), None)

        rmtree(tmp_dir)
