import unittest
import pipeline_tasks


class PipelineTasksTests(unittest.TestCase):
    def test_get_pipeline_task(self):
        pipeline_data = {
            'branch': 'master',
            'pipelines': [
                {
                    'build': [
                        'build'
                    ]
                },
            ],
            'tasks': [
                {
                    'build': {
                        'cmd': 'mvn clean install'
                    }
                },
            ]
        }

        self.assertEqual(pipeline_tasks.get_pipeline_tasks(pipeline_data.get('pipelines'), 'build'), ['build'])

    def test_get_tasks(self):
        pipeline_data = {
            'branch': 'master',
            'pipelines': [
                {
                    'build': [
                        'build'
                    ]
                },
            ],
            'tasks': [
                {
                    'build': {
                        'cmd': 'mvn clean install'
                    }
                },
            ]
        }

        self.assertEqual(pipeline_tasks.get_task(pipeline_data.get('tasks'), 'build'), {'cmd': 'mvn clean install'})
