AvenueCode Scripting Challenge
======

Simple Continuous Integration script to read a yml file and execute it's pipelines.

Script Requirements
======
*   Docker Environment:
    
    -   Docker
    
*   Local Host Environment:
    
    -   Python 3.5 or higher
    -   Maven
    -   Git
    -   Unzip
    -   Java


Script Usage Docker
======
Reminder: All commands must be executed within the root project folder

1. Build docker image: `docker build -t challenge .`
2. Run a docker container with: `docker run -v {PATH_TO}/scripting_challenge:/app -w /app challenge python3 avenue_code_ci.py --pipeline {PIPELINE_NAME} --repository {REPO_URL}`
    
Script Usage Local Host
======
Reminder: All commands must be executed within the root project folder

1. Run the following command: `python3 avenue_code_ci.py --pipeline {PIPELINE_NAME} --repository {REPO_URL}`
        
Script Arguments
======

*   PATH_TO (Docker only): Path to scripting-challenge dir to be used as container volume;
*   PIPELINE_NAME: the pipeline within pipeline.yml you want to execute;
*   REPO_URL: https://bitbucket.org/ac-recruitment/scripting-helloworld.git

Testing
=====
*You can test your script on your personal computer if you have `git`, `maven`, `unzip` and `java` installed.*

1. Running the tests inside localhost: `python3 -m unittest discover` 
2. Running the tests inside docker container: `docker run -v {PATH_TO}/scripting_challenge:/app -w /app challenge python3 -m unittest discover`